/*
 * tasks_lab_1.c
 *
 *  Created on: Jan 31, 2019
 *      Author: Maciej
 */
#include "tasks_lab_1.h"
#include "../Drivers/RTS_libs/FEAT_Scheduler/sch_basic_pub.h"

#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_hal_uart.h"
#include <string.h>

extern ADC_HandleTypeDef hadc1;
extern UART_HandleTypeDef huart2;



char str_LED_BLINK_1[]="LED1";
char str_LED_BLINK_2[]="LED2";
char str_RTR_CH_SWITCH[] = "RTR_CH_SWITCH_MMCR";

char str_TEST[] = "TEST 1\n";

#define LED_G_PORT GPIOA
#define LED_G_PIN	GPIO_PIN_5

void read_adc_loop();
void check_uart_loop();
void check_input_loop();

uint8_t adc_loop_id;
uint8_t uart_loop_id;
uint8_t input_loop_id;

uint8_t input_loop_id_overriden;
uint8_t adc_loop_id_overriden;
uint8_t uart_loop_id_overriden;

void lab1_power_up()
{
	adc_loop_id = SCH_NO_TIMEOUT_ID;
	uart_loop_id = SCH_NO_TIMEOUT_ID;
	input_loop_id = SCH_NO_TIMEOUT_ID;

}

/**
 * Sort by deadline, d
 */
void insertionSort(sch_loop_func_t task[], uint8_t period[],uint8_t execution[], uint8_t deadline[], int n)
{
	sch_loop_func_t key_task;
	int i, j, key_period, key_execution, key_deadline;
    for (i = 1; i < n; i++)
    {
    	key_task = task[i];
    	key_period = period[i];
    	key_execution = execution[i];
    	key_deadline = deadline[i];
        j = i - 1;

        /* Move elements of arr[0..i-1], that are
        greater than key, to one position ahead
        of their current position */
        while (j >= 0 && deadline[j] > key_deadline)
        {
        	task[j + 1] = task[j];
        	period[j + 1] = period[j];
        	execution[j + 1] = execution[j];
        	deadline[j + 1] = deadline[j];
            j = j - 1;
        }
        task[j + 1] = key_task;
        period[j + 1] = key_period;
        execution[j + 1] = key_execution;
        deadline[j + 1] = key_deadline;
    }
}


void lab1_init()
{
	timer_cb_test1(NULL);

	int p[] = {10,10,11};
	int e[] = {5,10,5};
	int d[] = {40,50,30};
	sch_loop_func_t tasks[] = {read_adc_loop,check_uart_loop,check_input_loop};
	int n = (int)(sizeof(d) / sizeof(d[0]));

	int frame_size;
	frame_size = find_frame_size( p, e, d );
	if(frame_size == -1){
		//frame size constraints not met
		return;
	}
	insertionSort(tasks, p, e, d, n);
	adc_loop_id = sch_add_loop(tasks[1]);
	uart_loop_id = sch_add_loop(tasks[2]);
	input_loop_id = sch_add_loop(tasks[0]);
}



void timer_cb_test1(uint8_t *x)
{
	HAL_GPIO_WritePin(LED_G_PORT,LED_G_PIN, 1); //Toggle LED
	sch_create_timeout(rtc_get_ticks()+1000, timer_cb_test2, 0, str_LED_BLINK_2);
}
void timer_cb_test2(uint8_t *x)
{
	HAL_GPIO_WritePin(LED_G_PORT, LED_G_PIN, 0); //Toggle LED
	sch_create_timeout(rtc_get_ticks()+1000, timer_cb_test1, 0, str_LED_BLINK_1);
}
///////////////////////////////////////////////


//HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart
//							, uint8_t *pData, uint16_t Size, uint32_t Timeout);
//HAL_StatusTypeDef HAL_UART_Receive(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

uint8_t msg1[] = "Triggered!";
uint8_t msg_help[] = "This code monitors for blue/user button trigger, and reads ADC1 when asked with letter 't'";



void check_input_loop()
{
	if (GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13))
	{
		if (HAL_OK != HAL_UART_Transmit(&huart2, msg1, strlen((char*)msg1), 5) )
			printf("Debug error while UART Tx");
		// 5 ticks ~= 5ms
	}
}

uint8_t buf[10];
uint16_t buf_len = 1; // reading one char at a time
void check_uart_loop()
{
	if (HAL_OK == HAL_UART_Receive(&huart2, buf, buf_len, 0))
	{
		// receive successful a byte
		if ((buf[0]=='t')||(buf[0] == 'T'))
		{
			// Start temperature reading from ADC
			HAL_ADC_Start(&hadc1);
		}
		if ((buf[0]=='h')||(buf[0]=='H')||(buf[0] == '?'))
		{
			// Start temperature reading from ADC
			HAL_UART_Transmit(&huart2, msg_help, strlen((char*)msg_help),10);
		}
	}
}


void read_adc_loop()
{
	if (HAL_OK == HAL_ADC_PollForConversion(&hadc1, 0))
	{
		// ADC ready
		char temp_str[15];
		uint32_t temp = HAL_ADC_GetValue(&hadc1);
		sprintf(temp_str, "T=%d", (int)temp);
		HAL_UART_Transmit(&huart2, (uint8_t*)temp_str, strlen(temp_str),5);
	}
}
